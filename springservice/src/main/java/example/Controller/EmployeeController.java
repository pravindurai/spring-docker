package example.Controller;

import example.dao.EmployeeDAO;
import example.models.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/company")
public class EmployeeController {

    @Autowired
    EmployeeDAO employeeDAO;

    /** Save an employee
     *a
     */
    @PostMapping("/employess")
    public Employee createEmployee(@RequestBody Employee emp){
        return employeeDAO.save(emp);
    }

    @GetMapping("/employees")
    public List<Employee> getAllEmployees(){
        return employeeDAO.findAll();
    }

    /** GET Employee By Id */

    @GetMapping("/employees/{id}")
    public ResponseEntity<Employee>  getEmployeeById(@PathVariable(value = "id") Long id){
        Employee emp = employeeDAO.findOne(id);

        if(emp == null){
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok().body(emp);
    }


    @PutMapping("/employees/{id}")
    public ResponseEntity<Employee>  updateEmployee(@PathVariable(value = "id") Long id, @Valid @RequestBody Employee empDetails){
        Employee emp = employeeDAO.findOne(id);

        if(emp == null){
            return ResponseEntity.notFound().build();
        }

        emp.setName(empDetails.getName());
        emp.setDesigination(empDetails.getDesigination());
        emp.setExpertise(empDetails.getExpertise());

        Employee employee = employeeDAO.save(emp);
        return ResponseEntity.ok().body(employee);
    }

    /** DELETE AN EMPLOYEE */
    @DeleteMapping("/employees/{id}")
    public ResponseEntity<Employee>  deleteEmployee(@PathVariable(value = "id") Long id){
        Employee emp = employeeDAO.findOne(id);

        if(emp == null){
            return ResponseEntity.notFound().build();
        }

        employeeDAO.delete(emp);
        return ResponseEntity.ok().build();

    }



}
