package example.dao;

import example.models.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import example.repository.EmployeeRepository;

import java.util.List;

@Service
public class EmployeeDAO {

    @Autowired
    EmployeeRepository employeeRepository;

    /** save an Employee */
    public Employee save(Employee emp){
        return employeeRepository.save(emp);
    }

    /** search all employee **/
    public List<Employee> findAll(){
        return employeeRepository.findAll();
    }

    /** find one employee */
    public Employee findOne(Long empId){
        return employeeRepository.getOne(empId);
    }

    /** delete an employee */
    public void delete(Employee emp){
        employeeRepository.delete(emp);
    }

}
