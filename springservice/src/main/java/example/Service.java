package example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@EnableJpaAuditing
@SpringBootApplication
@ComponentScan
public class Service {
    public static void main(String[] args) {

        String env = System.getenv("PRAVIN_ENV");
        SpringApplication.run(Service.class, args);
        System.out.println(env);
    }

}
